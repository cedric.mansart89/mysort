
public class MySort {

    public static int[] superSort(int[] arrayToSort) {

        boolean sorted = false;

        while (!sorted) {
            
            sorted = true;
            
            for (int i = 1; i < arrayToSort.length; i++) {

                var item = arrayToSort[i];

                if (arrayToSort[i - 1] > arrayToSort[i]) {
                    var tmp = arrayToSort[i];
                    arrayToSort[i] = arrayToSort[i - 1];
                    arrayToSort[i - 1] = tmp;
                    sorted = false;
                }
            }
        }
 return arrayToSort;
    }
}