
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class MySortTest {

    public MySortTest() {
    }

    @Test
    public void testSomeMethod() {
        int[] actual = MySort.superSort(new int[]{});
        Assertions.assertArrayEquals(new int[]{}, actual);
    }

    @Test
    public void testSomeMethod1() {
        int[] actual = MySort.superSort(new int[]{1});
        Assertions.assertArrayEquals(new int[]{1}, actual);
    }

    @Test
    public void testSomeMethod2() {
        int[] actual = MySort.superSort(new int[]{1, 2, 3, 4, 5});
        Assertions.assertArrayEquals(new int[]{1, 2, 3, 4, 5}, actual);
    }

    @Test
    public void testSomeMethod3() {
        int[] actual = MySort.superSort(new int[]{3, 5, 4, 1, 2});
        Assertions.assertArrayEquals(new int[]{1, 2, 3, 4, 5}, actual);

    }

    @Test
    public void testSomeMethod4() {
        int[] actual = MySort.superSort(new int[]{5, 4, 3, 2, 1});
        Assertions.assertArrayEquals(new int[]{1, 2, 3, 4, 5}, actual);
    }

    @Test
    public void testSomeMethod5() {
        int[] actual = MySort.superSort(new int[]{(-2), (-4), 3, 2, 1});
        Assertions.assertArrayEquals(new int[]{(-4), (-2), 1, 2, 3}, actual);
    }
}
